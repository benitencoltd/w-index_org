<?php
//ini_set( 'user_agent', 'User-Agent :' . $userAgent . PHP_EOL . 'x-up-subno :' . $subNo );
ini_set('user_agent', 'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)');

$url = array(
/*
'http://quotes.esignal.com/esignalprod/esigchartspon?cont=JGB10+1!-TPX&period=V&varminutes=5&size=737x300&bartype=CANDLE&bardensity=MEDIUM&showextendednames=true' => 'jgb',//JGB日本国債（10年）
*/

'http://www.fxempire.com/ChartDirector/php/freeforexchart.php?s2id_autogen1=&=&TickerSymbol=Japan%20Govt%20Bond&s2id_autogen2=&=&CompareWith=&TimeRange=5&ChartSize=L&Volume=&ParabolicSAR=0&LogScale=&PercentageScale=0&ChartType=CandleStick&Band=BB&avgType1=SMA&movAvg1=10&avgType2=SMA&movAvg2=50&Indicator1=RSI&Indicator2=MACD&Indicator3=None&Indicator4=None&Button1=&' => 'jgb',//JGB日本国債（10年）

'http://charts.quote.com/cis/fsspon?cont=JGB10+1!-TPX&period=D&size=365x189&bartype=CANDLE&bardensity=medium&headerbackground=%28221,221,221%29&headerforeground=%28102,102,102%29&headerdatacolor=%280,1,125%29&studyheaderbackground=%28221,221,221%29&showextendednames=true' => 'jgb-day',//JGB日本国債日足（10年）
'http://charts.marketcenter.com/cis/cbotcis?cont=ZN%20M7&period=V&bartype=CANDLE&bardensity=MEDIUM&showextendednames=true&headerbackground=255,255,255&headerforeground=0,0,0&headerdatacolor=0,0,0&size=737X300&varminutes=5' => 't-note',//J米10年国債（10年）
'http://charts.marketcenter.com/cis/cbotcis?cont=ZN%20H7&period=D&bartype=CANDLE&bardensity=MEDIUM&showextendednames=true&headerbackground=255,255,255&headerforeground=0,0,0&headerdatacolor=0,0,0&size=365X189' => 't-note-day',//米10年国債日足（10年）
);

foreach($url as $value => $filename){
    $handle = fopen("$value", "rb");
    $contents = stream_get_contents($handle);
    fclose($handle);

    $picname ="$filename".".png";
    $fw=fopen("$picname","w+");
    fwrite($fw,$contents);
    fclose($fw);

    $image = imagecreatefrompng($picname);
    //imagetruecolortopalette($image, false, 64);
    imagetruecolortopalette($image, false, 255);
    imagepng($image, $picname);
}

//ヤフーミニチャート米10年国債利回り
//	$handle = fopen("http://ichart.finance.yahoo.com/t?s=^TNX", "rb");
//ヤフーミニチャート米30年国債利回り
//	$handle = fopen("http://ichart.finance.yahoo.com/t?s=^TYX", "rb");
//ヤフーミニチャート米5年国債利回り
//	$handle = fopen("http://ichart.finance.yahoo.com/t?s=^FVX", "rb");
?>
