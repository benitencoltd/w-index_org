﻿<?php
$url = array(
//	'http://ichart.finance.yahoo.com/t?s=AA' => 'aa',//アルコア
	//'http://ichart.finance.yahoo.com/t?s=NKE' => 'nke',//ナイキ
	'https://www.google.com/finance/getchart?q=:NKE&x=NYSE&p=1d&i=60' => 'nke',//ナイキ | word
	//'http://ichart.finance.yahoo.com/t?s=AXP' => 'axp',//アメリカンエキスプレス
	'https://www.google.com/finance/getchart?q=AXP&x=NYSE&p=1d&i=60' => 'axp',//アメリカンエキスプレス	| word
	//'http://ichart.finance.yahoo.com/t?s=BA' => 'ba',//ボーイング
	'https://www.google.com/finance/getchart?q=BA&x=NYSE&p=1d&i=60' => 'ba',//ボーイング | word :
//	'http://ichart.finance.yahoo.com/t?s=BAC' => 'bac',//バンクオブアメリカ
	'http://ichart.finance.yahoo.com/t?s=GS' => 'gs',//ゴールドマン・サックス
	//'http://ichart.yahoo.com/t?s=CAT' => 'cat',//キャタピラー
	'https://www.google.com/finance/getchart?q=CAT&x=NYSE&p=1d&i=60' => 'cat',//キャタピラー | word: NYSE:CAT
	//'http://ichart.yahoo.com/t?s=CSCO' => 'csco',//シスコシステムズ
	'https://www.google.com/finance/getchart?q=CSCO&x=NASDAQ&p=1d&i=60' => 'csco',//シスコシステムズ | word
	//'http://ichart.yahoo.com/t?s=CVX' => 'cvx',//シェブロン
	'https://www.google.com/finance/getchart?q=CVX&x=NYSE&p=1d&i=60' => 'cvx',//シェブロン |word

	//'http://ichart.yahoo.com/t?s=DD' => 'dd',//デュポン
	'https://www.google.com/finance/getchart?q=DD&x=NYSE&p=1d&i=60' => 'dd',//デュポン |word NYSE:DD

	//'http://ichart.yahoo.com/t?s=DIS' => 'dis',//ウォルト・ディズニー
	'https://www.google.com/finance/getchart?q=DIS&x=NYSE&p=1d&i=60' => 'dis',//ウォルト・ディズニー |word
	//'http://ichart.yahoo.com/t?s=GE' => 'ge',//ゼネラルエレクトリック | word
	'https://www.google.com/finance/getchart?q=GE&x=NYSE&p=1d&i=60' => 'ge',//ゼネラルエレクトリック | word
	//'http://ichart.yahoo.com/t?s=HD' => 'hd',//ホームデポ
	'https://www.google.com/finance/getchart?q=HD&x=NYSE&p=1d&i=60' => 'hd',//ホームデポ |word
//	'http://ichart.yahoo.com/t?s=HPQ' => 'hpq',//ヒューレットパッカード
	//'http://ichart.yahoo.com/t?s=V' => 'v',//VISA
	'https://www.google.com/finance/getchart?q=V&x=NYSE&p=1d&i=60' => 'v',//VISA | word
	//'http://ichart.yahoo.com/t?s=IBM' => 'ibm',//IBM
	'https://www.google.com/finance/getchart?q=IBM&x=NYSE&p=1d&i=60' => 'ibm',//IBM | word
	//'http://ichart.yahoo.com/t?s=INTC' => 'intc',//インテル
	'https://www.google.com/finance/getchart?q=INTC&x=NASDAQ&p=1d&i=60' => 'intc',//インテル | word
	//'http://ichart.yahoo.com/t?s=JNJ' => 'jnj',//ジョンソンエンドジョンソン
	'https://www.google.com/finance/getchart?q=JNJ&x=NYSE&p=1d&i=60' => 'jnj',//ジョンソンエンドジョンソン |WORD
	//'http://ichart.yahoo.com/t?s=JPM' => 'jpm',//ＪＰモルガンチェース
	'https://www.google.com/finance/getchart?q=JPM&x=wordNYSE&p=1d&i=60' => 'jpm',//ＪＰモルガンチェース | word
	//'http://ichart.yahoo.com/t?s=UNH' => 'unh',//ユナイテッドヘルス
	'https://www.google.com/finance/getchart?q=UNH&x=NYSE&p=1d&i=60' => 'unh',//ユナイテッドヘルス |word
	//'http://ichart.yahoo.com/t?s=KO' => 'ko',//コカコーラ
	'https://www.google.com/finance/getchart?q=KO&x=NYSE&p=1d&i=60' => 'ko',//コカコーラ | word
	//'http://ichart.yahoo.com/t?s=MCD' => 'mcd',//マクドナルド
	'https://www.google.com/finance/getchart?q=MCD&x=NYSE&p=1d&i=60' => 'mcd',//マクドナルド |word NYSE:MCD
	//'http://ichart.yahoo.com/t?s=MMM' => 'mmm',//スリーエム
	'https://www.google.com/finance/getchart?q=MMM&x=NYSE&p=1d&i=60' => 'mmm',//スリーエム |word

	//'http://ichart.yahoo.com/t?s=MRK' => 'mrk',//メルク
	'https://www.google.com/finance/getchart?q=MRK&x=NYSE&p=1d&i=60' => 'mrk',//メルク |word
	//'http://ichart.yahoo.com/t?s=MSFT' => 'msft',//マイクロソフト
	'https://www.google.com/finance/getchart?q=MSFT&x=NASDAQ&p=1d&i=60' => 'msft',//マイクロソフト | word
	//'http://ichart.yahoo.com/t?s=PFE' => 'pfe',//ファイザー
	'https://www.google.com/finance/getchart?q=PFE&x=NYSE&p=1d&i=60' => 'pfe',//ファイザー |word
	//'http://ichart.yahoo.com/t?s=PG' => 'pg',//Ｐ＆Ｇ
	'https://www.google.com/finance/getchart?q=PG&x=NYSE&p=1d&i=60' => 'pg',//Ｐ＆Ｇ |WORD
	//'http://ichart.yahoo.com/t?s=T' => 't',//ＡＴ＆Ｔ
	'https://www.google.com/finance/getchart?q=T&x=NYSE&p=1d&i=60' => 't',//ＡＴ＆Ｔ | word us-stock NYSE:T
	//'http://ichart.yahoo.com/t?s=TRV' => 'trv',//トラベラーズ
	'https://www.google.com/finance/getchart?q=TRV&x=NYSE&p=1d&i=60' => 'trv',//トラベラーズ | word
	//'http://ichart.yahoo.com/t?s=UTX' => 'utx',//ユナイテッドテクノロジーズ
	'https://www.google.com/finance/getchart?q=UTX&x=NYSE&p=1d&i=60' => 'utx',//ユナイテッドテクノロジーズ |word :
	//'http://ichart.yahoo.com/t?s=VZ' => 'vz',//ベライゾン
	'https://www.google.com/finance/getchart?q=VZ&x=NYSE&p=1d&i=60' => 'vz',//ベライゾン  | word NYSE:VZ
	//'http://ichart.yahoo.com/t?s=WMT' => 'wmt',//ウォルマート
	'https://www.google.com/finance/getchart?q=WMT&x=NYSE&p=1d&i=60' => 'wmt',//ウォルマート |word
	//'http://ichart.yahoo.com/t?s=XOM' => 'xom',//エクソンモービル
	'https://www.google.com/finance/getchart?q=XOM&x=NYSE&p=1d&i=60' => 'xom',//エクソンモービル |word

	);

foreach($url as $value => $filename){
    if($handle = @fopen("$value", "rb")){

            $contents = stream_get_contents($handle);
            fclose($handle);

            $picname ="$filename".".png";
            $fw=fopen("$picname","w+");
            fwrite($fw,$contents);
            fclose($fw);

            $image = imagecreatefrompng($picname);
            imagetruecolortopalette($image, false, 64);
            imagepng($image, $picname);
    }
}
?>
