﻿<?php
$url = array(
	'https://www.google.com/finance/getchart?q=ATEYY&x=OTCMKTS&p=1d&i=60' => 'ate',//アドバンテスト
	//'https://www.google.com/finance/getchart?q=ATE&x=EPA&p=1d&i=60' => 'ate',//アドバンテスト |word->adr EPA:ATE
	//'http://ichart.yahoo.com/t?s=CAJ' => 'caj',//キヤノン
	'https://www.google.com/finance/getchart?q=CAJ&x=NYSE&p=1d&i=60' => 'caj',//キヤノン |work->adr
	//'http://ichart.finance.yahoo.com/t?s=DCM' => 'dcm',//NTTドコモ
	'https://www.google.com/finance/getchart?q=DCM&x=NYSE&p=1d&i=60' => 'dcm',//NTTドコモ | word->adr
	//'http://ichart.yahoo.com/t?s=HTHIY' => 'hit',//日立
	'https://www.google.com/finance/getchart?q=HTHIY&x=OTCMKTS&p=1d&i=60' => 'hit',//日立 | word->adr
	//'http://ichart.yahoo.com/t?s=HMC' => 'hmc',//ホンダ
	'https://www.google.com/finance/getchart?q=HMC&x=NYSE&p=1d&i=60' => 'hmc',//ホンダ | work->adr
	//'http://ichart.yahoo.com/t?s=IX' => 'ix',//オリックス
	'https://www.google.com/finance/getchart?q=IX&x=NYSE&p=1d&i=60' => 'ix',//オリックス |word->adr
	//'http://ichart.yahoo.com/t?s=KNM' => 'knm',//コナミ
	'https://www.google.com/finance/getchart?q=KNM&x=ASX&p=1d&i=60' => 'knm',//コナミ |word->adr  ASX:KNM
	//'http://ichart.yahoo.com/t?s=KUBTY' => 'kub',//クボタ
	'https://www.google.com/finance/getchart?q=KUBTY&x=OTCMKTS&p=1d&i=60' => 'kub',//クボタ  | word->ard OTCMKTS:KUBTY
	//'http://ichart.yahoo.com/t?s=KYO' => 'kyo',//京セラ
	'https://www.google.com/finance/getchart?q=KYO&x=NYSE&p=1d&i=60' => 'kyo',//京セラ | word->adr
	//'http://ichart.yahoo.com/t?s=PCRFY' => 'pc',//パナソニック
	'https://www.google.com/finance/getchart?q=PCRFY&x=OTCMKTS&p=1d&i=60' => 'pc',//パナソニック | word->adr
	//'http://ichart.yahoo.com/t?s=MFG' => 'mfg',//みずほ
	'https://www.google.com/finance/getchart?q=MFG&x=ASX&p=1d&i=60' => 'mfg',//みずほ |word->adr
	//'http://ichart.yahoo.com/t?s=MTU' => 'mtu',//三菱UFJ
	'https://www.google.com/finance/getchart?q=MTU&x=NYSE&p=1d&i=60' => 'mtu',//三菱UFJ |word->adr NYSE:MTU
	//'http://ichart.yahoo.com/t?s=NJ' => 'nj',//日本電産
	'https://www.google.com/finance/getchart?q=NJDCY&x=OTCMKTS&p=1d&i=60' => 'nj',//日本電産 |word->adr
	//'http://ichart.finance.yahoo.com/t?s=NMR' => 'nmr',//野村
	'https://www.google.com/finance/getchart?q=NMR&x=NYSE&p=1d&i=60' => 'nmr',//野村 | word->adr NYSE:NMR

	//'http://ichart.yahoo.com/t?s=NTT' => 'ntt',//NTT
	'https://www.google.com/finance/getchart?q=NTTYY&x=OTCMKTS&p=1d&i=60' => 'ntt',//NTT  | word->adr  OTCMKTS:NTTYY
	//'http://ichart.yahoo.com/t?s=SNE' => 'sne',//ソニー
	'https://www.google.com/finance/getchart?q=SNE&x=NYSE&p=1d&i=60' => 'sne',//ソニー |word->adr
	//'http://ichart.yahoo.com/t?s=TM' => 'tm',//トヨタ
	'https://www.google.com/finance/getchart?q=TM&x=NYSE&p=1d&i=60' => 'tm',//トヨタ | word->adr
	//'http://ichart.yahoo.com/t?s=IIJI' => 'iiji',//インターネットイニシアティブ
	'https://www.google.com/finance/getchart?q=IIJI&x=NASDAQ&p=1d&i=60' => 'iiji',//インターネットイニシアティブ |word->adr NASDAQ:IIJI
	//'http://ichart.finance.yahoo.com/t?s=MKTAY' => 'mktay',//マキタ
	'https://www.google.com/finance/getchart?q=MKTAY&x=OTCMKTS&p=1d&i=60' => 'mktay',//マキタ | word->adr
	//'http://ichart.finance.yahoo.com/t?s=WACLY' => 'wacly',//ワコール
	'https://www.google.com/finance/getchart?q=WACLY&x=OTCMKTS&p=1d&i=60' => 'wacly',//ワコール |word->adr OTCMKTS:WACLY

	//'http://ichart.finance.yahoo.com/t?s=AAPL' => 'aapl',//アップル | word
	'https://www.google.com/finance/getchart?q=AAPL&x=NASDAQ&p=1d&i=60' => 'aapl',//アップル | word NASDAQ:AAPL
	//'http://ichart.finance.yahoo.com/t?s=AAPL' => 'aapl',//アップル

	//'http://ichart.finance.yahoo.com/t?s=GOOG' => 'goog',//グーグル | word -> us-stock
	'https://www.google.com/finance/getchart?q=GOOG&x=NASDAQ&p=1d&i=60' => 'goog',//グーグル | word -> us-stock

	//'http://ichart.finance.yahoo.com/t?s=YHOO' => 'yhoo',//ヤフー
	'https://www.google.com/finance/getchart?q=YHOO&x=NASDAQ&p=1d&i=60' => 'yhoo',//ヤフー |word =>us-stock
	//'http://ichart.finance.yahoo.com/t?s=EBAY' => 'ebay',//イーベイ
	'https://www.google.com/finance/getchart?q=EBAY&x=NASDAQ&p=1d&i=60' => 'ebay',//イーベイ |word =>us-tock
	//'http://ichart.yahoo.com/t?s=AMZN' => 'amzn',//アマゾン・ドットコム
	'https://www.google.com/finance/getchart?q=AMZN&x=NASDAQ&p=1d&i=60' => 'amzn',//アマゾン・ドットコム | word->us-stock
	//'http://ichart.yahoo.com/t?s=ADBE' => 'adbe',//アドビ
	'https://www.google.com/finance/getchart?q=ADBE&x=NASDAQ&p=1d&i=60' => 'adbe',//アドビ | word->us-stock NASDAQ:ADBE
	//'http://ichart.yahoo.com/t?s=SYMC' => 'symc',//シマンテック
	'https://www.google.com/finance/getchart?q=SYMC&x=NASDAQ&p=1d&i=60' => 'symc',//シマンテック | word->us-stock
	//'http://ichart.yahoo.com/t?s=ORCL' => 'orcl',//オラクル
	'https://www.google.com/finance/getchart?q=ORCL&x=NYSE&p=1d&i=60' => 'orcl',//オラクル | word ->us-stock
	//'http://ichart.yahoo.com/t?s=TXN' => 'txn',//テキサスインスツルメンツ
	'https://www.google.com/finance/getchart?q=TXN&x=NASDAQ&p=1d&i=60' => 'txn',//テキサスインスツルメンツ |word-us-stock
	//'http://ichart.yahoo.com/t?s=DELL' => 'dell',//デル (上場廃止)
	'https://www.google.com/finance/getchart?q=DELL&x=NASDAQ&p=1d&i=60' => 'dell',//テキサスインスツルメンツ |word-us-stock

	//'http://ichart.yahoo.com/t?s=NVDA' => 'nvda',//エヌビディア
	'https://www.google.com/finance/getchart?q=NVDA&x=NASDAQ&p=1d&i=60' => 'nvda',//エヌビディア | word->us-stock

	//'http://ichart.yahoo.com/t?s=AMD' => 'amd',//AMD
	'https://www.google.com/finance/getchart?q=AMD&x=NASDAQ&p=1d&i=60' => 'amd',//AMD | word->us-stock
	//'http://ichart.yahoo.com/t?s=C' => 'c',//シティグループ
	'https://www.google.com/finance/getchart?q=C&x=NYSE&p=1d&i=60' => 'c',//シティグループ | word->us-stock

	//'http://ichart.yahoo.com/t?s=GS' => 'gs',//ゴールドマンサックス | word | word->us-stock
	'https://www.google.com/finance/getchart?q=GS&x=NYSE&p=1d&i=60' => 'gs',//ゴールドマンサックス | word
	///'http://ichart.yahoo.com/t?s=MS' => 'ms',//モルガンスタンレー
	'https://www.google.com/finance/getchart?q=MS&x=NYSE&p=1d&i=60' => 'ms',//モルガンスタンレー | word->us-stock
	//'http://ichart.yahoo.com/t?s=BK' => 'bk',//バンクオブニューヨークメロン NYSE:BK
	'https://www.google.com/finance/getchart?q=BK&x=NYSE&p=1d&i=60' => 'bk',//バンクオブニューヨークメロン | word->us-stock
	//'http://ichart.yahoo.com/t?s=BRK-B' => 'brk-b',//バークシャー・ハザウェイ NYSE:BRK.B
	'https://www.google.com/finance/getchart?q=BRK.B&x=NYSE&p=1d&i=60' => 'brk-b',//バークシャー・ハザウェイ |word-us-stock
	//'http://ichart.yahoo.com/t?s=GM' => 'gm',//ゼネラルモーターズ
	'https://www.google.com/finance/getchart?q=GM&x=NYSE&p=1d&i=60' => 'gm',//ゼネラルモーターズ |wrod->us-stock
	//'http://ichart.yahoo.com/t?s=F' => 'f',//フォード
	'https://www.google.com/finance/getchart?q=F&x=NYSE&p=1d&i=60' => 'f',//フォード | word->us-stock
	//'http://ichart.yahoo.com/t?s=SBUX' => 'sbux',//スターバックス
	'https://www.google.com/finance/getchart?q=SBUX&x=NASDAQ&p=1d&i=60' => 'sbux',//スターバックス |word->us-stock
	'http://ichart.yahoo.com/t?s=HPQ' => 'hpq',//ヒューレットパッカード
	);

foreach($url as $value => $filename){
    if($handle = @fopen("$value", "rb")){
        $contents = stream_get_contents($handle);
        fclose($handle);

        $picname ="$filename".".png";
        $fw=fopen("$picname","w+");
        fwrite($fw,$contents);
        fclose($fw);

        $image = imagecreatefrompng($picname);
        imagetruecolortopalette($image, false, 64);
        imagepng($image, $picname);
    }
}
?>
