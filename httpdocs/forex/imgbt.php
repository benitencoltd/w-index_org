<?php
//http://freeserv.dukascopy.com:8080/ChartServer/chart?stock_id=509&interval=60&points_number=100&view_type=line&width=258&height=120
//↑ID書き換えるだけ。
$url = array(
	'https://www.google.com/finance/getchart?q=USDJPY&p=1d&i=60' => 'usdjpy',//USDJPY
	'https://www.google.com/finance/getchart?q=EURJPY&p=1d&i=60' => 'eurjpy',//EURJPY
	'https://www.google.com/finance/getchart?q=GBPJPY&p=1d&i=60' => 'gbpjpy',//GBPJPY
	'https://www.google.com/finance/getchart?q=AUDJPY&p=1d&i=60' => 'audjpy',//AUDJPY
	'https://www.google.com/finance/getchart?q=NZDJPY&p=1d&i=60' => 'nzdjpy',//NZDJPY
	'https://www.google.com/finance/getchart?q=EURUSD&p=1d&i=60' => 'eurusd',//EURUSD
	'https://www.google.com/finance/getchart?q=GBPUSD&p=1d&i=60' => 'gbpusd',//GBPUSD
	'https://www.google.com/finance/getchart?q=CADJPY&p=1d&i=60' => 'cadjpy',//CADJPY
	'https://www.google.com/finance/getchart?q=CHFJPY&p=1d&i=60' => 'chfjpy',//CHFJPY
	'https://www.google.com/finance/getchart?q=USDCHF&p=1d&i=60' => 'usdchf',//USDCHF
	'https://www.google.com/finance/getchart?q=AUDUSD&p=1d&i=60' => 'audusd',//AUDUSD
	'https://www.google.com/finance/getchart?q=EURGBP&p=1d&i=60' => 'eurgbp',//EURGBP
	'https://www.google.com/finance/getchart?q=HKDJPY&p=1d&i=60' => 'hkdjpy',//HKDJPY
	'https://www.google.com/finance/getchart?q=CNYJPY&p=1d&i=60' => 'cnyjpy',//CNYJPY
	'https://www.google.com/finance/getchart?q=KRWJPY&p=1d&i=60' => 'krwjpy',//KRWJPY
	'https://www.google.com/finance/getchart?q=THBJPY&p=1d&i=60' => 'thbjpy',//THBJPY
	'https://www.google.com/finance/getchart?q=SGDJPY&p=1d&i=60' => 'sgdjpy',//SGDJPY
	'https://www.google.com/finance/getchart?q=TRYJPY&p=1d&i=60' => 'tryjpy',//TRYJPY(トルコリラ円)
	'https://www.google.com/finance/getchart?q=EURDKK&p=1d&i=60' => 'eurdkk',//EURDKK(ユーロデンマーククローネ)
	'https://www.google.com/finance/getchart?q=EURRUB&p=1d&i=60' => 'eurrub',//EURRUB(ユーロロシアルーブル)
	'https://www.google.com/finance/getchart?q=EURRON&p=1d&i=60' => 'eurron',//EURRON(ユーロルーマニアレウ)
	'https://www.google.com/finance/getchart?q=USDHKD&p=1d&i=60' => 'usdhkd',//USDHKD(米ドル香港ドル)
	'https://www.google.com/finance/getchart?q=USDCNY&p=1d&i=60' => 'usdcny',//USDCNY(米ドル人民元)
	'https://www.google.com/finance/getchart?q=NZDUSD&p=1d&i=60' => 'nzdusd',//NZDUSD(NZドル米ドル)
	'https://www.google.com/finance/getchart?q=USDCAD&p=1d&i=60' => 'usdcad',//USDCAD(米ドルカナダドル)
	'https://www.google.com/finance/getchart?q=USDMXN&p=1d&i=60' => 'usdmxn',//USDMXN(米ドルメキシコペソ)
	'https://www.google.com/finance/getchart?q=USDSGD&p=1d&i=60' => 'usdsgd',//USDSGD(米ドルシンガポールドル)
	'https://www.google.com/finance/getchart?q=USDPLN&p=1d&i=60' => 'usdpln',//USDPLN(米ドルポーランドズロチ)
	'https://www.google.com/finance/getchart?q=USDTRY&p=1d&i=60' => 'usdtry',//USDTRY(米ドルトルコリラ)
	'https://www.google.com/finance/getchart?q=USDHUF&p=1d&i=60' => 'usdhuf',//USDHUF(米ドルハンガリーフォント)
	'https://www.google.com/finance/getchart?q=EURAUD&p=1d&i=60' => 'euraud',//EURAUD(ユーロ豪ドル)
	'https://www.google.com/finance/getchart?q=EURCAD&p=1d&i=60' => 'eurcad',//EURCAD(ユーロカナダドル)
	'https://www.google.com/finance/getchart?q=EURNOK&p=1d&i=60' => 'eurnok',//EURNOK(ユーロノルウェークローネ)
	'https://www.google.com/finance/getchart?q=EURCHF&p=1d&i=60' => 'eurchf',//EURCHF(ユーロスイスフラン)
	'https://www.google.com/finance/getchart?q=EURNZD&p=1d&i=60' => 'eurnzd',//EURNZD(ユーロNZドル)
	'https://www.google.com/finance/getchart?q=EURPLN&p=1d&i=60' => 'eurpln',//EURPLN(ユーロポーランドズロチ)
	'https://www.google.com/finance/getchart?q=EURSEK&p=1d&i=60' => 'eursek',//EURSEK(ユーロスウェーデンクローナ)
	'https://www.google.com/finance/getchart?q=EURSGD&p=1d&i=60' => 'eursgd',//EURSGD(ユーロシンガポールドル)
	'https://www.google.com/finance/getchart?q=EURTRY&p=1d&i=60' => 'eurtry',//EURTRY(ユーロトルコリラ)
	'https://www.google.com/finance/getchart?q=GBPAUD&p=1d&i=60' => 'gbpaud',//GBPAUD(ポンド豪ドル)
	'https://www.google.com/finance/getchart?q=GBPCAD&p=1d&i=60' => 'gbpcad',//GBPCAD(ポンドカナダドル)
	'https://www.google.com/finance/getchart?q=GBPCHF&p=1d&i=60' => 'gbpchf',//GBPCHF(ポンドスイスフラン)
	'https://www.google.com/finance/getchart?q=GBPNZD&p=1d&i=60' => 'gbpnzd',//GBPNZD(ポンドNZドル)
	'https://www.google.com/finance/getchart?q=AUDCAD&p=1d&i=60' => 'audcad',//AUDCAD(豪ドルカナダドル)
	'https://www.google.com/finance/getchart?q=AUDCHF&p=1d&i=60' => 'audchf',//AUDCHF(豪ドルスイスフラン)
	'https://www.google.com/finance/getchart?q=AUDNZD&p=1d&i=60' => 'audnzd',//AUDNZD(豪ドルNZドル)
	);

foreach($url as $value => $filename){
	$handle = fopen("$value", "rb");
	$contents = stream_get_contents($handle);
	fclose($handle);

	$picname ="$filename".".png";
	$fw=fopen("$picname","w+");
	fwrite($fw,$contents);
	fclose($fw);

	$image = imagecreatefrompng($picname);
	imagetruecolortopalette($image, false, 64);
	imagepng($image, $picname);
	}
?>
