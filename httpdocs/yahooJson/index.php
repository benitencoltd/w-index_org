<?php
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Origin: http://www.w-index.com');

$symbol = $_GET['symbol'];
$range = isset($_GET['range'])?$_GET['range']:'1d';
echo get_data($symbol,$range);

function get_data($symbol,$range) {
    $url = "https://partner-query.finance.yahoo.com/v8/finance/chart/$symbol?range=$range&comparisons=undefined&includePrePost=false&interval=1m&corsDomain=stocks.finance.yahoo.co.jp";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $data = curl_exec($curl);
    curl_close($curl);

    return $data;
}
?>