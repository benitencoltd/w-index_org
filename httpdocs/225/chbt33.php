<?php
//ini_set( 'user_agent', 'User-Agent :' . $userAgent . PHP_EOL . 'x-up-subno :' . $subNo );

$url = array(
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=321&15%3A00' => 'suisan',//水産・農林業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=322&15%3A00' => 'kougyou',//鉱業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=323&15%3A00' => 'kensetu',//建設業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=324&15%3A00' => 'shokuryouhin',//食料品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=325&15%3A00' => 'senni',//繊維製品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=326&15%3A00' => 'kami',//パルプ・紙
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=327&15%3A00' => 'kagaku',//化学
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=328&15%3A00' => 'iyakuhin',//医薬品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=329&15%3A00' => 'sekiyusekitan',//石油・石炭製品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=330&15%3A00' => 'gomu',//ゴム製品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=331&15%3A00' => 'garasu',//硝子・土石製品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=332&15%3A00' => 'tekkou',//鉄鋼
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=333&15%3A00' => 'hitetu',//非鉄金属
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=334&15%3A00' => 'kinnzoku',//金属製品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=335&15%3A00' => 'kikai',//機械
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=336&15%3A00' => 'denki',//電気機器
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=337&15%3A00' => 'yusou',//輸送用機器
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=338&15%3A00' => 'seimitu',//精密機器
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=339&15%3A00' => 'sonota',//その他製品
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=340&15%3A00' => 'denkigasu',//電気ガス業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=341&15%3A00' => 'rikuun',//陸運業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=342&15%3A00' => 'kaiun',//海運業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=343&15%3A00' => 'kuuun',//空運業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=344&15%3A00' => 'souko',//倉庫・運輸関連業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=345&15%3A00' => 'jyouhoutuusin',//情報・通信業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=346&15%3A00' => 'orosiuri',//卸売業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=347&15%3A00' => 'kouri',//小売業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=348&15%3A00' => 'ginkou',//銀行業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=349&15%3A00' => 'shouken',//証券、商品先物
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=350&15%3A00' => 'hoken',//保険業
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=351&15%3A00' => 'kinyuu',//その他金融
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=352&15%3A00' => 'fudousan',//不動産
	'http://quote.jpx.co.jp/jpx/chart/chart23.exe?template=ini/realindex&basequote=353&15%3A00' => 'service',//サービス業
);

foreach($url as $value => $filename){
    $handle = fopen("$value", "rb");
    $contents = stream_get_contents($handle);
    fclose($handle);

    $picname ="$filename".".gif";
    $fw=fopen("$picname","w+");
    fwrite($fw,$contents);
    fclose($fw);

    $image = imagecreatefromgif($picname);
    imagetruecolortopalette($image, false, 64);
    imagegif($image, $picname);
}

?>