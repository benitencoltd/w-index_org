﻿<?php
$url = array(
	'http://ichart.finance.yahoo.com/t?s=CLV10.NYM&lang=en-US&region=US' => 'oil',//原油
	'http://ichart.finance.yahoo.com/t?s=HOV10.NYM&lang=en-US&region=US' => 'heatingoil',//灯油
	'http://ichart.finance.yahoo.com/t?s=NGV10.NYM&lang=en-US&region=US' => 'naturalgas',//天然ガス
	'http://ichart.finance.yahoo.com/t?s=RBV10.NYM&lang=en-US&region=US' => 'gasoline',//ガソリン
	'http://ichart.finance.yahoo.com/t?s=GCZ10.CMX&lang=en-US&region=US' => 'gold',//金
	'http://ichart.finance.yahoo.com/t?s=SIZ10.CMX&lang=en-US&region=US' => 'silver',//銀
	'http://ichart.finance.yahoo.com/t?s=HGZ10.CMX&lang=en-US&region=US' => 'copper',//銅
	'http://ichart.finance.yahoo.com/t?s=PLV10.NYM&lang=en-US&region=US' => 'platinum',//プラチナ
	'http://ichart.finance.yahoo.com/t?s=PAZ10.NYM&lang=en-US&region=US' => 'palladium',//パラジウム
	'http://ichart.finance.yahoo.com/t?s=CZ10.CBT&lang=en-US&region=US' => 'corn',//トウモロコシ
	'http://ichart.finance.yahoo.com/t?s=WZ10.CBT&lang=en-US&region=US' => 'wheat',//小麦
	'http://ichart.finance.yahoo.com/t?s=SX10.CBT&lang=en-US&region=US' => 'soybeans',//大豆
	'http://ichart.finance.yahoo.com/t?s=LCV10.CME&lang=en-US&region=US' => 'livecattle',//生牛
	'http://ichart.finance.yahoo.com/t?s=LHV10.CME&lang=en-US&region=US' => 'leanhogs',//赤身豚肉
	'http://ichart.finance.yahoo.com/t?s=CCZ10.NYB&lang=en-US&region=US' => 'cocoa',//ココア
	'http://ichart.finance.yahoo.com/t?s=KCU10.NYB&lang=en-US&region=US' => 'coffee',//コーヒー
	'http://ichart.finance.yahoo.com/t?s=CTZ10.NYB&lang=en-US&region=US' => 'cotton',//綿
	'http://ichart.finance.yahoo.com/t?s=SBV10.NYB&lang=en-US&region=US' => 'sugar',//砂糖
	);

foreach($url as $value => $filename){
	$handle = fopen("$value", "rb");
	$contents = stream_get_contents($handle);
	fclose($handle);

	$picname ="$filename".".png";
	$fw=fopen("$picname","w+");
	fwrite($fw,$contents);
	fclose($fw);

	$image = imagecreatefrompng($picname);
	imagetruecolortopalette($image, false, 64);
	imagepng($image, $picname);
	}
?>